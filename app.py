from flask import Flask, render_template, request, redirect, url_for, send_file, send_from_directory

import json
import os
import uuid
import logging

app = Flask(__name__)

# Configure logging
logging.basicConfig(level=logging.DEBUG)

@app.route('/')
def main():
    return render_template('main.html')

# @app.route('/submit-json', methods=['POST'])
# def submit_json():
#     data = request.get_json()
#     try:
#         populated_index_html = render_template('index.html', **data)
#         filename = f"index_{uuid.uuid4().hex}.html"
#         with open(filename, 'w') as index_file:
#             index_file.write(populated_index_html)
#         return {'success': True, 'filename': filename}
#     except Exception as e:
#         logging.error(f"Error processing JSON: {e}")
#         return {'success': False, 'error': str(e)}
@app.route('/submit-json', methods=['POST'])
def submit_json():
    data = request.get_json()
    try:
        populated_index_html = render_template('index.html', **data)
        filename = f"index_{uuid.uuid4().hex}.html"
        with open(filename, 'w') as index_file:
            index_file.write(populated_index_html)
        return {'success': True, 'filename': filename}
    except Exception as e:
        logging.error(f"Error processing JSON: {e}")
        return {'success': False, 'error': str(e)}

@app.route('/generated/<filename>')
def serve_generated_file(filename):
    return send_from_directory('.', filename)

if __name__ == '__main__':
    app.run(debug=True)
