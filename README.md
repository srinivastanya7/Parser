# Project README

## Project Setup and Usage

This guide will assist you in setting up and utilizing the project.

### Steps to Run the Project

1. **Open Terminal:** Open a terminal window on your computer.



2. **Start the Application:** Run the application by executing the following command:

    ```bash
    python app.py
    ```

3. **Access the Application:** Open a web browser and go to `http://localhost:5000` (or the port number displayed in your terminal).

## Adding JSON Data

To add JSON data for verification, follow this sample structure:

```json
{
  "projectName": "My Project",
  "taskDescription": "Verification Task Description",
  "currentDate": "2023-04-01",
  "totalFilesAnalyzed": 100,
  "potentialIssuesFound": 5,
  "verificationStatus": "Pass",
  "files": [
    {
      "filePath": "path/to/file1.txt",
      "issueDescription": "Issue description for file1",
      "issueType": "Type A",
      "issueCategory": "Category 1",
      "priority": "High"
    },
    {
      "filePath": "path/to/file2.txt",
      "issueDescription": "Issue description for file2",
      "issueType": "Type B",
      "issueCategory": "Category 2",
      "priority": "Medium"
    },
    {
      "filePath": "path/to/file3.txt",
      "issueDescription": "Issue description for file3",
      "issueType": "Type C",
      "issueCategory": "Category 3",
      "priority": "Low"
    }
  ],
  "additionalNotes": "Additional notes here",
  "issueType1": "Type A",
  "briefExplanationIssueType1": "Explanation of Type A",
  "issueType2": "Type B",
  "briefExplanationIssueType2": "Explanation of Type B"
}

